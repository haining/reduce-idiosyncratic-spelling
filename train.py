# coding=utf-8


import os
import torch
import wandb
import datasets
import numpy as np
from itertools import chain
import pytorch_lightning as pl
from utilities import calculate_common_metrics
from pytorch_lightning.plugins import DDPPlugin
from torch.utils.data import Dataset, DataLoader
from pytorch_lightning.loggers import WandbLogger
from pytorch_lightning.callbacks import (ModelCheckpoint,
                                         LearningRateMonitor,
                                         EarlyStopping,
                                         DeviceStatsMonitor,
                                         TQDMProgressBar)
from pytorch_lightning.strategies import DeepSpeedStrategy
from transformers import T5TokenizerFast, T5ForConditionalGeneration

# env variable
os.environ["WANDB_CACHE_DIR"] = "./wandb"
# os.environ["WANDB_DISABLE_GIT"] = "False"
# os.environ["WANDB_DISABLE_CODE"] = "False"
os.environ["HF_DATASETS_CACHE"] = "./huggingface"
# os.environ["WANDB_ENTITY"] = "False"
# constants
MODEL_NAME = 'google/t5-efficient-tiny-nl32'
# MODEL_NAME = 'google/t5-efficient-base-nl24'
CKPT_PATH = None
# CKPT_PATH = 'ckpts_google/t5-efficient-base-nl24/epoch=00-global_step=0-val_loss=0.08-val_sacrebleu=95.48-v1.ckpt'

NYT_DATASET_PATH = 'resource/nyt_dataset_hf'
CKPTS_DIR = f"ckpts_{MODEL_NAME}"
PROJECT_NAME = "reduce_idiosyncratic_spelling"
RUN_NAME = f"{MODEL_NAME}"
MAX_SOURCE_LENGTH = 256
MAX_TARGET_LENGTH = 256
NUM_WORKERS = 0

# to log generated texts
PATIENCE = 3000
BATCH_SIZE = 24
NUM_LOG_TEXT_ROWS = 50
LOG_TEXT_INTERVAL = 10000
DECODING_STRATEGIES = ['greedy', 'beam==4', 'top_p==.9']


class NYTDataset(Dataset):
    def __init__(
            self,
            data: list,  # a list of dicts of paired texts, keyed by `source_lang` and `target_lang`
            tokenizer: str = MODEL_NAME,
            max_source_length: int = 256,
            max_target_length: int = 256,
            source_lang: str = 'en_noisy',
            target_lang: str = 'en_clean'):
        self.data = data
        self.tokenizer = T5TokenizerFast.from_pretrained(tokenizer)
        self.max_source_length = max_source_length
        self.max_target_length = max_target_length
        self.source_lang = source_lang
        self.target_lang = target_lang

    def __len__(self):
        return len(self.data)

    def __getitem__(self, index: int):
        datum = self.data[index]
        target_text = datum[self.target_lang]
        source_text = datum[self.source_lang]
        encoding = self.tokenizer(source_text,
                                  max_length=self.max_source_length,
                                  padding='max_length',
                                  truncation=True,
                                  return_tensors='pt'
                                  )
        target_encoding = self.tokenizer(target_text,
                                         max_length=self.max_source_length,
                                         padding='max_length',
                                         truncation=True)
        # replace all tokenizer.pad_token_id in the labels by -100 when we want to ignore padding in the loss
        labels = torch.tensor(
            [(label if label != self.tokenizer.pad_token_id else -100) for label in target_encoding['input_ids']],
            dtype=torch.int64)

        return dict(
            input_ids=encoding['input_ids'].flatten(),
            attention_mask=encoding['attention_mask'].flatten(),
            labels=labels.flatten(),
            target_lang=self.target_lang,
            source_lang=self.source_lang,
            target_text=target_text,
            source_text=source_text,
        )


class NYTDataModule(pl.LightningDataModule):
    """
    DataModule for a denoising autoencoder used for misspellings correction.
    It loads huggingface format dataset ('datasets.dataset_dict.DatasetDict') from disk and return train/val/test
    dataloaders.
    """

    def __init__(
            self,
            tokenizer: str = MODEL_NAME,
            nyt_dataset_folder: str = 'resource/nyt_dataset_hf',
            batch_size: int = BATCH_SIZE,
            max_source_length: int = MAX_SOURCE_LENGTH,
            max_target_length: int = MAX_TARGET_LENGTH,
            source_lang: str = 'en_noisy',
            target_lang: str = 'en_clean',
            num_workers: int = NUM_WORKERS,
    ):
        super().__init__()
        self.dataset = datasets.load_from_disk(nyt_dataset_folder)
        self.batch_size = batch_size
        self.tokenizer = tokenizer
        self.max_source_length = max_source_length
        self.max_target_length = max_target_length
        self.source_lang = source_lang
        self.target_lang = target_lang
        self.num_workers = num_workers

    def setup(self, stage=None):
        self.train_dataset = NYTDataset(
            self.dataset['train']['translation'],
            self.tokenizer,
            self.max_source_length,
            self.max_target_length,
            self.source_lang,
            self.target_lang)

        self.val_dataset = NYTDataset(
            self.dataset['validation']['translation'],
            self.tokenizer,
            self.max_source_length,
            self.max_target_length,
            self.source_lang,
            self.target_lang)

        self.test_dataset = NYTDataset(
            self.dataset['test']['translation'],
            self.tokenizer,
            self.max_source_length,
            self.max_target_length,
            self.source_lang,
            self.target_lang)

    def train_dataloader(self):
        return DataLoader(
            self.train_dataset,
            batch_size=self.batch_size,
            shuffle=True,
            num_workers=self.num_workers
        )

    def val_dataloader(self):
        return DataLoader(
            self.val_dataset,
            batch_size=self.batch_size,
            shuffle=False,
            num_workers=self.num_workers
        )

    def test_dataloader(self):
        return DataLoader(
            self.test_dataset,
            batch_size=self.batch_size,
            shuffle=False,
            num_workers=self.num_workers
        )


class DenoisingAutoencoderModel(pl.LightningModule):
    """
    A denoising autoencoder built for correcting mis-/idiosyncratic spellings.
    """

    def __init__(self,
                 model_name: str = MODEL_NAME,
                 tokenizer: str = MODEL_NAME,
                 monitor: bool = False,
                 ):
        super().__init__()
        self.model = T5ForConditionalGeneration.from_pretrained(model_name)
        self.save_hyperparameters()
        self.tokenizer = T5TokenizerFast.from_pretrained(tokenizer)
        self.monitor = monitor

    def forward(self, input_ids,
                attention_mask,
                labels=None):
        output = self.model(
            input_ids=input_ids,
            attention_mask=attention_mask,
            labels=labels)

        return output.loss, output.logits

    def training_step(self, batch, batch_size):
        input_ids = batch['input_ids']
        attention_mask = batch['attention_mask']
        labels = batch['labels']

        loss, logits = self(
            input_ids=input_ids,
            attention_mask=attention_mask,
            labels=labels
        )
        self.log("train_loss", loss, batch_size=batch_size)

        return loss

    def validation_step(self, batch, batch_size):
        input_ids = batch['input_ids']
        attention_mask = batch['attention_mask']
        labels = batch['labels']

        loss, logits = self(
            input_ids=input_ids,
            attention_mask=attention_mask,
            labels=labels
        )
        # the output of self() is: odict_keys(['loss', 'logits', 'past_key_values', 'encoder_last_hidden_state'])
        self.log("val_loss", loss, batch_size=batch_size)

        # compute metrics only when
        if self.monitor:
            if self.global_step % LOG_TEXT_INTERVAL == 0:
                self._generate_step(batch, 'val')

        return loss

    def _generate_step(self, batch, data_split: str):
        assert data_split in ['val', 'test']
        # generate given decoding strategy and compute metrics
        source_texts = batch['source_text']
        target_texts = [[r.strip()] for r in batch['target_text']]  # a list of lists

        # log decoding
        for decoding_strategy in DECODING_STRATEGIES:
            decoding = decoding_strategy.split('==')[0]
            # assert decoding in ['greedy', 'beam', 'top_p']:
            if decoding == 'greedy':
                output_sequences = self.model.generate(
                                input_ids=batch["input_ids"],
                                attention_mask=batch["attention_mask"],
                                max_length=MAX_TARGET_LENGTH,
                                do_sample=False)
            elif decoding == 'beam':
                output_sequences = self.model.generate(
                                input_ids=batch["input_ids"],
                                attention_mask=batch["attention_mask"],
                                max_length=MAX_TARGET_LENGTH,
                                num_beams=int(decoding_strategy.split('==')[1]),
                                do_sample=False)
            elif decoding == 'top_p':
                output_sequences = self.model.generate(
                                input_ids=batch["input_ids"],
                                attention_mask=batch["attention_mask"],
                                max_length=MAX_TARGET_LENGTH,
                                top_p=float(decoding_strategy.split('==')[1]),
                                do_sample=True)
            # compute common metrics: sacrebleu and meteor
            predictions = [p.strip() for p in self.tokenizer.batch_decode(output_sequences, skip_special_tokens=True)]
            metrics = calculate_common_metrics(predictions, target_texts, ['sacrebleu', 'meteor'])
            # logging
            wandb_logger.log_metrics({f"{data_split}/sacrebleu_{decoding_strategy}": metrics['sacrebleu']})
            wandb_logger.log_metrics({f"{data_split}/meteor_{decoding_strategy}": metrics['meteor']})
            wandb_logger.log_text(key=f"{data_split}_generated_texts_{decoding_strategy}/global_step_{self.global_step}",
                                  columns=[f'generated_texts_{decoding_strategy}', 'source_texts', 'target_texts'],
                                  data=[*zip(predictions,
                                             source_texts,
                                             list(chain(*target_texts)))]) # [:NUM_LOG_TEXT_ROWS]

    def test_step(self, batch, batch_size):
        input_ids = batch['text_input_ids']
        attention_mask = batch['text_attention_mask']
        labels = batch['labels']

        loss, outputs = self(
            input_ids=input_ids,
            attention_mask=attention_mask,
            labels=labels
        )
        # the output of self() is: odict_keys(['loss', 'logits', 'past_key_values', 'encoder_last_hidden_state'])
        self.log("test_loss", loss, batch_size=batch_size)
        if self.monitor:
            self._generate_step(batch, 'test')

        return loss

    def configure_optimizers(self):
        return torch.optim.AdamW(self.parameters(), lr=1e-4)


if __name__ == '__main__':

    pl.seed_everything(0)

    # init logger
    wandb_logger = WandbLogger(name=RUN_NAME,
                               project=PROJECT_NAME,
                               log_model='all',
                               save_dir='wandb')

    # prepare the data
    # convert nyt corpus into datasets.dataset_dict.DatasetDict
    # only needs to do once
    # nyt = datasets.load_dataset('json', data_files={'train': 'resource/nyt_train_0.jsonl',
    #                                        'validation': 'resource/nyt_validation.jsonl',
    #                                        'test': 'resource/nyt_test.jsonl'})
    # nyt.save_to_disk(NYT_DATASET_PATH)
    dm = NYTDataModule(tokenizer=MODEL_NAME)

    # prepare the model
    # if use a ckpt
    if CKPT_PATH:
        model = DenoisingAutoencoderModel.load_from_checkpoint(checkpoint_path=CKPT_PATH)
    else:
        model = DenoisingAutoencoderModel(model_name=MODEL_NAME,
                                          tokenizer=MODEL_NAME)

    # prepare the trainer
    trainer = pl.Trainer(
        # fast dev
        # fast_dev_run=500,
        # deepspeed
        # accelerator="gpu",
        # devices=4,
        # strategy="deepspeed_stage_1",
        # precision=16,
        # strategy=DeepSpeedStrategy(stage=3, logging_batch_size_per_gpu=BATCH_SIZE),
        # gpu
        accelerator="gpu",
        devices=[0, 1, 2, 3],
        strategy='ddp',
        # plugins=DDPPlugin(find_unused_parameters=False),
        # step
        max_epochs=1,
        log_every_n_steps=100,
        val_check_interval=2000,
        callbacks=[ModelCheckpoint(
            dirpath=CKPTS_DIR,
            every_n_train_steps=1000,
            filename='{step}-{val_loss:.3f}',
            save_top_k=5,
            verbose=True,
            monitor='val_loss',
            mode='min'),
            LearningRateMonitor("step"),
            EarlyStopping(monitor="val_loss", min_delta=1e-3, patience=PATIENCE, verbose=False,
                          mode="min"),
            DeviceStatsMonitor(),
            TQDMProgressBar(refresh_rate=1)],
        # misc
        deterministic=True,
        enable_progress_bar=True,
        move_metrics_to_cpu=True,
        logger=wandb_logger,
        default_root_dir=CKPTS_DIR,
    )

    trainer.fit(model=model, datamodule=dm)

#!/usr/bin/env python
# coding=utf-8

"""
This module
1. reads in clean NYT corpus from the English Gigaword-3 corpus.
2. splits 500 dev and 500 test samples from training samples.
3. injects character noise into the fixed dev and test splits
4. makes three copies of training data with injected character noise
5. saves files into .jsonl format as per to convention in Datasets library.

"""

__author__ = "hw56@indiana.edu"
__version__ = "1.0.0"
__license__ = "ISC"


from sklearn.model_selection import train_test_split
from utilities import (list2jsonl,
                       generate_clean_noise_pair,
                       extract_clean_subcorpus_from_en_gigaword3)

# how many copies of training
NUM_TRAIN_COPY = 3
SEED = 42

# path to English Gigaword3 corpus
gigaword_dir = '../english-gigaword_3/'
# save dirs890
train_file_paths = ['resource/nyt_corpus/nyt_train_' + str(i) + ".jsonl" for i in range(NUM_TRAIN_COPY)]
val_file_path = 'resource/nyt_corpus/nyt_validation.jsonl'
test_file_path = 'resource/nyt_corpus/nyt_test.jsonl'


if __name__ == '__main__':
    print("*" * 100)
    print("Starts extracting the New York Times subset from English Gigaword3...")
    # extract clean nyt corpus from gigaword
    clean_texts = extract_clean_subcorpus_from_en_gigaword3(agency_name='nyt', corpus_dir=gigaword_dir)
    # split data_dict list into train/dev/test
    train_data, val_test_data = train_test_split(clean_texts, test_size=4000, random_state=SEED)
    val_data, test_data = train_test_split(val_test_data, test_size=2000, random_state=SEED)
    print("Injects noise to clean data...")
    # inject noise and save to disk with multiprocessing
    list2jsonl(generate_clean_noise_pair(val_data), val_file_path)
    list2jsonl(generate_clean_noise_pair(test_data), test_file_path)
    for train_file_path in train_file_paths:
        list2jsonl(generate_clean_noise_pair(train_data), train_file_path)
    print("Generated data splits are saved.")
    print("*" * 100)

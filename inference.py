# coding=utf-8

import datasets
import numpy as np
from collections import Counter
from sacremoses import MosesTokenizer
from transformers import T5TokenizerFast
from utilities import calculate_common_metrics
from train import DenoisingAutoencoderModel, MAX_TARGET_LENGTH, MAX_SOURCE_LENGTH

# load the best ckpt
MODEL_NAME = 'google/t5-efficient-base-nl24'
# CKPT_PATH = 'ckpts_google/t5-efficient-tiny-nl32/epoch=00-global_step=00-val_loss=0.14-val_sacrebleu=92.63.ckpt'
# CKPT_PATH = 'ckpts_google/t5-efficient-base-nl24/epoch=00-global_step=0-val_loss=0.08-val_sacrebleu=95.65.ckpt'
CKPT_PATH = 'ckpts_google/t5-efficient-base-nl24/step=121000-val_loss=0.117.ckpt'


# model = DenoisingAutoencoderModel.load_from_checkpoint(CKPT_PATH)
# model.freeze()
# tokenizer = T5TokenizerFast.from_pretrained(MODEL_NAME)


def denoise_text(noisy_texts: list,
                 decoding_strategy: str = 'greedy',
                 ckpt_path: str = CKPT_PATH,
                 tokenizer: str = MODEL_NAME,
                 max_source_length: str = MAX_SOURCE_LENGTH,
                 max_target_length: str = MAX_TARGET_LENGTH):
    """
    Corrects misspellings found in English string.
    Args:
        noisy_texts: a list of str, texts need to be denoised.
        decoding_strategy: str, formatted as one of the following: ['greedy', 'top_p==0.9', 'beam==4', 'top_k==50']
        ckpt_path: str, path to a trained pl model
        tokenizer: str, a t5 model name to be used in .from_pretrained()
        max_source_length: int, default to MAX_SOURCE_LENGTH
        max_target_length: int, default to MAX_TARGET_LENGTH

    Returns:
        list of str, English string without misspellings
    """

    assert decoding_strategy.split('==')[0] in ['greedy', 'top_p', 'beam', 'top_k']
    decoding = decoding_strategy.split('==')[0]

    model = DenoisingAutoencoderModel.load_from_checkpoint(ckpt_path)
    model.freeze()
    tokenizer = T5TokenizerFast.from_pretrained(tokenizer)

    batch = tokenizer(
        [t.strip() for t in noisy_texts],
        max_length=max_source_length,
        padding='max_length',
        truncation=True,
        return_tensors='pt')

    if decoding == 'top_p':
        assert 1e-8 < float(decoding_strategy.split('==')[1]) < 1.0
        output_sequences = model.model.generate(
            input_ids=batch["input_ids"],
            attention_mask=batch["attention_mask"],
            max_length=max_target_length,
            top_p=float(decoding_strategy.split('==')[1]),
            do_sample=True)
    elif decoding == 'top_k':
        assert 0 < int(decoding_strategy.split('==')[1]) < 5000
        output_sequences = model.model.generate(
            input_ids=batch["input_ids"],
            attention_mask=batch["attention_mask"],
            max_length=max_target_length,
            top_k=int(decoding_strategy.split('==')[1]),
            do_sample=True)
    elif decoding == 'beam':
        assert 0 < int(decoding_strategy.split('==')[1]) < 200
        output_sequences = model.model.generate(
            input_ids=batch["input_ids"],
            attention_mask=batch["attention_mask"],
            max_length=max_target_length,
            num_beams=int(decoding_strategy.split('==')[1]),
            do_sample=False)
    elif decoding == 'greedy':
        output_sequences = model.model.generate(
            input_ids=batch["input_ids"],
            attention_mask=batch["attention_mask"],
            max_length=max_target_length,
            do_sample=False)
    generated_texts = [p.strip() for p in tokenizer.batch_decode(output_sequences,
                                                                 skip_special_tokens=True)]
    return generated_texts


def calculate_correction_accuracy(misspellings,
                                  correct_spellings,
                                  noisy_texts,
                                  generated_texts,
                                  references):
    # sanity check
    assert len(misspellings) == len(correct_spellings) == len(noisy_texts) == len(generated_texts) == len(references)
    moses_tokenizer = MosesTokenizer(lang='en')
    for i in range(len(misspellings)):
        # make sure misspellings are not in `references` and in `noisy_texts`
        assert misspellings[i] not in moses_tokenizer.tokenize(references[i][0], escape=False), 'a'
        assert misspellings[i] in moses_tokenizer.tokenize(noisy_texts[i], escape=False), f'{misspellings[i]}'
        # make sure correct spellings are in `references` and not in `noisy_texts`
        assert correct_spellings[i] in moses_tokenizer.tokenize(references[i][0], escape=False), 'c'
        assert correct_spellings[i] not in moses_tokenizer.tokenize(noisy_texts[i], escape=False), 'd'
        # if misspellings[i] in references[i][0]:
        #     print(f'scenario a: {misspellings[i]}')
        # if misspellings[i] not in noisy_texts[i]:
        #     print(f'scenario b: {misspellings[i]}')
        # # make sure correct spellings are in `references` and not in `noisy_texts`
        # if correct_spellings[i] not in references[i][0]:
        #     print(f'scenario c: {misspellings[i]}')
        # if correct_spellings[i] in noisy_texts[i]:
        #     print(f'scenario d: {misspellings[i]}')
    # count how many misspellings are corrected overall misspellings
    total_correction = 0
    total_miss = 0
    for i in range(len(misspellings)):
        num_misspellings = Counter(moses_tokenizer.tokenize(noisy_texts[i], escape=False))[misspellings[i]]
        # `num_misspellings` cannot be 0 because it has passed sanity check
        if num_misspellings == 1:
            num_correction = Counter(moses_tokenizer.tokenize(references[i][0], escape=False))[correct_spellings[i]]
            if num_correction == 0:
                total_miss += 1
            elif num_correction == 1:
                total_correction += 1
            else:
                raise RuntimeError(f'More than 1 correction happened, check out {misspellings[i]}.')
    print(f"total_correction is {total_correction}.")
    print(f"total_miss is {total_miss}.")


# texts-containing-misspelled-words-v2.txt
v2_path = 'texts-containing-misspelled-words-v2.txt'
content = [l for l in open(v2_path, 'r').read().splitlines() if not (l.startswith('#') or l == '')]
test_misspellings = content[::4]
test_correct_spellings = content[1::4]
test_noisy_texts = content[2::4]
test_clean_texts = content[3::4]
corrected_texts = denoise_text(test_noisy_texts, 'greedy')
metrics = calculate_common_metrics(corrected_texts,
                                   [[t] for t in test_clean_texts],
                                   ['sacrebleu', 'meteor'])

calculate_correction_accuracy(misspellings=test_misspellings,
                              correct_spellings=test_correct_spellings,
                              noisy_texts=test_noisy_texts,
                              generated_texts=corrected_texts,
                              references=[[t] for t in test_clean_texts])









# 1. first 20 GIGA3-NYT testing samples, looks good
nyt_test_dataset = datasets.load_dataset('json', data_files={'test': 'resource/nyt_corpus/nyt_test.jsonl'})
nyt_test_dataset_first20_noisy = [t['en_noisy'] for t in nyt_test_dataset['test'][:5]['translation']]
nyt_test_dataset_first20_clean = [t['en_clean'] for t in nyt_test_dataset['test'][:5]['translation']]
result = denoise_text(noisy_texts=nyt_test_dataset_first20_noisy,
                      references=nyt_test_dataset_first20_clean,
                      frozen_pl_model=model)

# 2. first 20 GIGA3-NYT testing samples, failed
american_english_samples = [
    'The film is often credited with making Amitabh Bachchan a "superstar," two years after he became a star with Zanjeer (1973).']
british_english_samples = [
    'The film is often credited with making Amitabh Bachchan a "superstar", two years after he became a star with Zanjeer (1973).']
result = denoise_text(noisy_texts=british_english_samples,
                      references=american_english_samples,
                      frozen_pl_model=model)

# 3. texts-containing-misspelled-words-v2.txt
v2_path = 'texts-containing-misspelled-words-v2.txt'
content = [l for l in open(v2_path, 'r').read().splitlines() if not (l.startswith('#') or l == '')]
test_misspellings = content[::4]
test_correct_spellings = content[1::4]
test_noisy_texts = content[2::4]
test_clean_texts = content[3::4]
result = denoise_text(noisy_texts=test_noisy_texts,
                      references=test_clean_texts,
                      misspellings=test_misspellings,
                      correct_spellings=test_correct_spellings,
                      frozen_pl_model=model)
